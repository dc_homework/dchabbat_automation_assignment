"""
this module contains herokuap_menu_page - The page object for the page we are doing the test on
"""
from selenium.webdriver.common.by import By


class HerokuapMenu:
    URL = 'https://the-internet.herokuapp.com/context_menu'

    BODY = (By.CSS_SELECTOR, 'body')

    def __init__(self, browser):
        self.browser = browser

    def load(self):
        self.browser.get(self.URL)
