import selenium as selenium
from pytest import fixture
from selenium import webdriver


@fixture()
def get_url():
    return 'https://the-internet.herokuapp.com/context_menu'


@fixture()
def browser():
    b = selenium.webdriver.Chrome('/Users/dvorah/PycharmProjects/dchabbat_automation_assignment/driver/chromedriver')
    b.implicitly_wait(10)
    yield b
    b.quit()
