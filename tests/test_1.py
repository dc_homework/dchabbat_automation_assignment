import pytest
import logging
from pages.herokuap_menu_page import HerokuapMenu


"""
1. Test should PASS if the string “ Right-click in the box below to see one called 'the-internet' “ found on page.
2. Test should FAIL if string “Alibaba” is not found on the same page.
"""
log = logging.getLogger('log')


def get_string_to_verify():
    return ['Right-click in the box below to see one called \'the-internet\'', 'Alibaba']


@pytest.mark.parametrize('string_to_check', get_string_to_verify())
def test_confirm_string(browser, string_to_check):
    page_to_search = HerokuapMenu(browser)
    page_to_search.load()
    #browser.get('https://the-internet.herokuapp.com/context_menu')
    bodyText = browser.find_element_by_css_selector('body').text
    print('text to find: ' + bodyText)
    assert string_to_check in bodyText
